package com.cybersolution.fazeal.test.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.cybersolution.fazeal.controller.ImageInterfaceController;
import com.cybersolution.fazeal.models.ERole;
import com.cybersolution.fazeal.models.ImageModel;
import com.cybersolution.fazeal.models.Role;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.service.ImageService;
import com.cybersolution.fazeal.service.UserService;
import com.cybersolution.fazeal.service.impl.Dropbox;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ImageListControllerTest {
	@InjectMocks
	private ImageInterfaceController imageController;

	@Mock
	private ImageService imageService;

	@Mock
	private UserService userService;

	@Mock
	private Dropbox dropbox;

	@Mock
	public RestTemplate restTemplate;


	@Test
	public void deleteImageFailTestCase() throws Exception {
		Set<Role> userRoles = new HashSet<Role>();
		userRoles.add(new Role(null, ERole.ROLE_USER));
		User user = new User("testUser", "test@test.com", "Encrepted", false);

		ImageModel image1 = new ImageModel(1L, (new Date()), "jpg", user,"path");
		ImageModel image2 = new ImageModel(2L, (new Date()), "jpg", user,"path2");
		List<ImageModel> images = new ArrayList<ImageModel>();
		images.add(image1);
		images.add(image2);
		
		List<ImageModel> expected = List.copyOf(images);

		when(imageService.listAll()).thenReturn(images);

		ResponseEntity<List<ImageModel>> listAllImages = imageController.listAllImages();
		assertEquals(expected, listAllImages.getBody());
	}


}
