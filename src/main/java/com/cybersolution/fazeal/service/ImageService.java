package com.cybersolution.fazeal.service;

import java.util.List;

import com.cybersolution.fazeal.models.ImageModel;

public interface ImageService {
	
	List<ImageModel> listAll();

	List<ImageModel> findByUserId(Integer id);

	ImageModel saveImage(ImageModel image);

	boolean delete(Long id);
}
