package com.cybersolution.fazeal.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cybersolution.fazeal.models.ShoppingList;
import com.cybersolution.fazeal.repository.ShoppingListRepository;
import com.cybersolution.fazeal.service.ShoppingListService;


@Service
public class ShoppingListServiceImpl implements ShoppingListService {
	
	@Autowired
	ShoppingListRepository shopListRepo;
	
	@Override
	public ShoppingList findById(Integer id) {
		return shopListRepo.getById(id);
	}

	@Override
	public ShoppingList save(ShoppingList shopList) {
		ShoppingList finalList = shopListRepo.save(shopList);
		return finalList;
	}

	@Override
	public ShoppingList getShoppingListForFamilyOnId(Integer familyId) {
		return shopListRepo.getShoppingListForFamilyOnId(familyId);
	}

	@Override
	public ShoppingList delete(ShoppingList shopList) {
		shopListRepo.delete(shopList);
		return shopList;
	}

}
