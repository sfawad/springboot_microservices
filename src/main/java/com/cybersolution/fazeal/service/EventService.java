package com.cybersolution.fazeal.service;

import java.util.List;

import com.cybersolution.fazeal.models.Events;


public interface EventService {
	
	boolean updateEvent(Integer id, String event, boolean isShared);
	
	List<Events> findByUserId(Integer userId);
}
