package com.cybersolution.fazeal.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity 
@Table(name ="events_user_family")
public class EventsFamilyUser {
	
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private Integer id;
	
	 	@ManyToOne
	    @JoinColumn(name = "user_id")
	    private User user;

	    @ManyToOne
	    @JoinColumn(name = "family_id")
	    private Family family;

	    @ManyToOne
	    @JoinColumn(name = "event_id")
	    private Events events;

		public EventsFamilyUser(User user, Family family, Events events) {
			this.user = user;
			this.family = family;
			this.events = events;
		}
		
		public EventsFamilyUser() {
		}
}
