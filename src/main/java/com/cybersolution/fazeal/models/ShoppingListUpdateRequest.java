package com.cybersolution.fazeal.models;

import java.util.Map;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
 

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ShoppingListUpdateRequest {

    private Integer id;
    
    @NotBlank
    @Size(min = 3, max = 100)
    private String shoppingListName;

	private boolean listStatus;
	
	// Shopping list items
	private Map<String,Boolean> itemMap;
	
	private Integer userId;

}
