package com.cybersolution.fazeal.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cybersolution.fazeal.exception.ResourceNotFoundException;
import com.cybersolution.fazeal.models.ERole;
import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.FamilyUserRoles;
import com.cybersolution.fazeal.models.MessageResponse;
import com.cybersolution.fazeal.models.Role;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.repository.FamilyUserRoleRespository;
import com.cybersolution.fazeal.repository.RoleRepository;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.service.FamilyService;
import com.cybersolution.fazeal.service.impl.Messages;

/**
 * @author Faizan
 * Family controller for fetching family details, saving and updating 
 */
@CrossOrigin(origins = "*", maxAge = 4300)
@RestController
@RequestMapping("/api/v1/family")
public class FamilyController {

	private static final Logger LOGGER = LogManager.getLogger(FamilyController.class);

	@Autowired
	FamilyService famService;
	
	@Autowired
	Messages messages;
	
	@Autowired
	UserRepository userRepo;
	
	@Autowired
	RoleRepository roleRepo;
	
	@Autowired
	FamilyUserRoleRespository familyUsersRoleRepo;
	
	/**
	 * @apiNote Used to get family by id
	 * @Param Integer id
	 * @return ResponseEntity (Family)
	 *
	 */
	@GetMapping("/byId/{id}")
	public ResponseEntity getFamilyById(@PathVariable(value = "id") Integer id) {
		LOGGER.traceEntry("getFamilyById -> "+id);
		Family family = famService.findById(id);
		if(null == family) {
			return ResponseEntity.badRequest().body(
					new MessageResponse(messages.get("ERROR_FAMILY_NAME_INVALID")));
		}
		LOGGER.info("getFamilyById(): " + family);
		LOGGER.traceExit();
		return new ResponseEntity(family, HttpStatus.OK);
	}
	
	/**
	 * @apiNote Used to get family by name
	 * @Param String name
	 * @return ResponseEntity (Family)
	 *
	 */
	@GetMapping("/byName/{name}")
	public ResponseEntity getFamilyByName(@PathVariable(value = "name") String name) {
		LOGGER.traceEntry("getFamilyByName -> "+name);
		Family family = famService.getFamilyInfoFromName(name);
		if (null == family) {
			return ResponseEntity.badRequest().body(
					new MessageResponse(messages.get("ERROR_FAMILY_NOTFOUND") + name));
		}
		LOGGER.info("getFamilyByName(): " + family);
		LOGGER.traceExit();
		return new ResponseEntity(family, HttpStatus.OK);
	}

	/**
	 * @apiNote Used to update family by id
	 * @param Integer id
	 * @Param String info
	 * @Param Long phone
	 * @Param String address1
	 * @Param String address2
	 * @Param String city
	 * @Param String state
	 * @Param String zip
	 * @Param String country
	 * @return ResponseEntity (Family)
	 *
	 */
	@PutMapping("/update/byId/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity updateFamilyInfo(@PathVariable(value = "id") Integer familyId,
			@RequestParam("info") String familyInfo, @RequestParam("phone") Long phone,
			@RequestParam("address1") String address1, @RequestParam("address2") String address2,
			@RequestParam("city") String city, @RequestParam("state") String state, @RequestParam("zip") String zip,
			@RequestParam("country") String country) {
		
		LOGGER.traceEntry("updateFamilyInfo -> "+familyId+ " "+familyInfo+ " "+phone
				+" "+address1+ " "+address2+ " "+city+ " "+state+ " "+zip
				+" "+country);
		Family family = famService.getById(familyId);
		if (null == family) {
			return ResponseEntity.badRequest().body(
					new MessageResponse(messages.get("ERROR_FAMILY_NOTFOUND_UPDATE") +familyId));
		}
			if (null != familyInfo)
				family.setInfo(familyInfo);

			if (null != address1)
				family.setAddress1(address1);

			if (null != address2)
				family.setAddress2(address2);

			if (null != city)
				family.setCity(city);

			if (null != state)
				family.setState(state);

			if (null != zip)
				family.setZip(zip);

			if (null != country)
				family.setCountry(country);

			if (null != phone)
				family.setPhone(phone);

			famService.save(family);

		LOGGER.info("updateFamilyInfo(): " + family);
		LOGGER.traceExit();
		return ResponseEntity.ok(family);

	}
	
	/**
	 * @apiNote Used to save family
	 * @param String name
	 * @Param String info
	 * @Param Long phone
	 * @Param String address1
	 * @Param String address2
	 * @Param String city
	 * @Param String state
	 * @Param String zip
	 * @Param String country
	 * @Param String relation
	 * @return ResponseEntity (Family)
	 *
	 */
	@PutMapping("/save/userId/{id}")
	public ResponseEntity saveFamilyInfo(@PathVariable("id") Integer id,@RequestParam("name") String familyName,
			@RequestParam("info") String familyInfo, @RequestParam("phone") Long phone,
			@RequestParam("address1") String address1, @RequestParam("address2") String address2,
			@RequestParam("city") String city, @RequestParam("state") String state, @RequestParam("zip") String zip,
			@RequestParam("country") String country,@RequestParam("relationWithFamily") String relation) {
		
		LOGGER.traceEntry("saveFamilyInfo -> "+familyName+ " "+familyInfo+ " "+phone
				+" "+address1+ " "+address2+ " "+city+ " "+state+ " "+zip
				+" "+country);
		if (StringUtils.isEmpty(familyName))
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get("ERROR_FAMILY_MANDATORY")));
		if (StringUtils.isEmpty(relation))
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get("ERROR_RELATION_MANDATORY")));
		
		User user  = userRepo.findById(id).get();
		if(null == user)
		{
			return ResponseEntity.badRequest().body(new MessageResponse(messages.get("ERROR_USER_NOT_FOUND") + id));
		}
		
		Family family = famService.getFamilyInfoFromName(familyName);
		if (null == family) {
			
			  family = new Family( familyName, familyInfo, phone, address1, address2,
			  city, state, zip, country); 
			  
			  famService.save(family);
			  
			  Set<Family> families = new HashSet<>();
			  families.add(family);
			  
			  Set<Role> roles = new HashSet<>();
			  Role userRole = roleRepo.findByName(ERole.ROLE_ADMIN).get();
			  if(null != userRole)
			  roles.add(userRole);
			  
			  user.setRoles(roles);
			  user.setFamilies(families);
			  user.setEnabled(true);
			  userRepo.save(user);
			  
			  // Add user role to ROLE_ADMIN for the creator of the family in app.
			  FamilyUserRoles usersRoles = new FamilyUserRoles(family.getFamilyId(), user.getId(), userRole.getName().toString(),relation);
			  familyUsersRoleRepo.save(usersRoles);
		} else {
			return ResponseEntity.badRequest().body(
					new MessageResponse(messages.get("ERROR_FAMILY_EXISTS")));
		}

		LOGGER.info("updateFamilyInfo(): " + family);
		LOGGER.traceExit();
		return ResponseEntity.ok(family);
	}
	
	/**
	 * @apiNote Used to request to join a family
	 * @param Integer userId 
	 * @Param String familyName
	 * @return ResponseEntity (Family)
	 */
	@PutMapping("/requestFamily/join/{id}")
	public ResponseEntity requestJoinUserToFamily(@PathVariable("id") Integer userId,
			@RequestParam("familyname") String familyName) {
		
		LOGGER.traceEntry("requestJoinUserToFamily -> "+familyName+ " "+userId);
		if (StringUtils.isEmpty(familyName))
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get("ERROR_FAMILY_MANDATORY")));
		
		if (null == userId)
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get("ERROR_USER_NULL")));
		
		Family family = famService.getFamilyInfoFromName(familyName);
		if (null != family) {
			User user  = userRepo.findById(userId).get();
			if(null != user)
			{	
				user.setPassword("");
				Set<User> pending_users = family.getPendingUser();
				pending_users.add(user);
				family.setPendingUser(pending_users);
				famService.save(family);
			}else {
				return ResponseEntity.badRequest()
						.body(new MessageResponse(messages.get("ERROR_USER_NOT_FOUND")));
			}
		} else {
			return ResponseEntity.badRequest().body(
					new MessageResponse(messages.get("ERROR_FAMILY_NOTFOUND")));
		}

		LOGGER.info("requestJoinUserToFamily(): " + family);
		LOGGER.traceExit();
		return ResponseEntity.ok(new MessageResponse(messages.get("FAMILY_JOINING_REQUEST")));

	}
	
	/**
	 * @apiNote Admin rights required for user to accept the incoming joining request by another user
	 * @param Integer userId 
	 * @Param String familyName
	 * @return ResponseEntity (Family)
	 */
	@PutMapping("/associateUser/user/{id}")
	public ResponseEntity associateUserToFamily(@PathVariable("id") Integer userId,
			@RequestParam("name") String familyName, @RequestParam("relationToFamily") String relationToFamily) {
		
		LOGGER.traceEntry("associateUserToFamily -> "+familyName+ " "+userId);
		if (StringUtils.isEmpty(familyName))
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get("ERROR_FAMILY_MANDATORY")));
		
		if (null == userId)
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get("ERROR_USER_NULL")));
		
		if (StringUtils.isEmpty(relationToFamily))
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get("ERROR_RELATION_MANDATORY")));
		
		
		Family family = famService.getFamilyInfoFromName(familyName);
		if (null != family) {
			User user  = userRepo.findById(userId).get();
			if(null != user && family.getPendingUser().contains(user))
			{	
				Set<User> pending_users = family.getPendingUser();
				for(User users : pending_users) {
					if(users.getId().equals(userId)) {
						pending_users.remove(users);
						break;
					}
				}
				famService.save(family);
				
				Set<Family> families = user.getFamilies();
				families.add(family);
				user.setFamilies(families);
				user.setEnabled(true);
				userRepo.save(user);
				
				// logic to update user to ROLE_USER, if somehow it has already an entry for the same family or add.
				Role userRole = roleRepo.findByName(ERole.ROLE_USER).get();
				FamilyUserRoles rolesOfUser = familyUsersRoleRepo.findRoleForUserInFamily(family.getFamilyId(), userId);
				if(null == rolesOfUser) {
					 FamilyUserRoles usersRoles = new FamilyUserRoles(family.getFamilyId(), user.getId(), userRole.getName().toString(),relationToFamily);
					 familyUsersRoleRepo.save(usersRoles);
				}else {
					rolesOfUser.setRole(userRole.getName().toString());
				}
				
			}else {
				return ResponseEntity.badRequest()
						.body(new MessageResponse(messages.get("ERROR_USER_NOT_FOUND_FOR_APPROVAL")));
			}
		} else {
			return ResponseEntity.badRequest().body(
					new MessageResponse(messages.get("ERROR_FAMILY_NOTFOUND")));
		}

		LOGGER.info("updateFamilyInfo(): " + family);
		LOGGER.traceExit();
		return ResponseEntity.ok(family);
	}
	
	/**
	 * @apiNote Admin rights required for user to disassociate user from family
	 * @param Integer userId 
	 * @Param String familyName
	 * @param Intger userIdToRemove
	 * @return ResponseEntity (Family)
	 *
	 */
	@PutMapping("/disassociateUser/user/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity disassociateUser(@PathVariable("id") Integer userId,
			@RequestParam("name") String familyName, @RequestParam("userIdToRemove") Integer userIdToRemove) {
		
		LOGGER.traceEntry("disassociateUser -> "+familyName+ " "+userId + " "+userIdToRemove);
		if (StringUtils.isEmpty(familyName))
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get("ERROR_FAMILY_MANDATORY")));
		
		if (null == userIdToRemove)
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get("ERROR_USER_NULL")));
		
		Family family = famService.getFamilyInfoFromName(familyName);
		if (null != family) {
			User user  = userRepo.findById(userIdToRemove).get();
			if(null != user)
			{
				Set<Family> families = user.getFamilies();
				for(Family familia : families) {
					if(familia.getFamilyName().equalsIgnoreCase(familyName)) {
						families.remove(familia);
						break;
					}
				}

				user.setFamilies(families);
				userRepo.save(user);
			}else {
				return ResponseEntity.badRequest()
						.body(new MessageResponse(messages.get("ERROR_USER_NOT_FOUND")));
			}
		} else {
			return ResponseEntity.badRequest().body(
					new MessageResponse(messages.get("ERROR_FAMILY_NOTFOUND")));
		}

		LOGGER.info("disassociateUser(): " + family);
		LOGGER.traceExit();
		return ResponseEntity.ok(family);
	}

	/**
	 * @apiNote Admin rights required to update user role in family
	 * @param Integer userId 
	 * @Param Integer familyId
	 * @param Intger userIdToBeUpdated
	 * @param Boolean isAdmin = if u want to update user to admin or demote to user
	 * @return ResponseEntity (Family)
	 *
	 */
	@PutMapping("/updateUserFamilyRole/{id}")
	public ResponseEntity<?> updateUserFamilyRole(@PathVariable(value = "id") Integer userId,
			@RequestParam("isAdmin") boolean isAdmin, @RequestParam("userIdToBeUpdated") Integer userIdToBeUpdated,@RequestParam("familyId") Integer familyId)
			throws ResourceNotFoundException {
		LOGGER.traceEntry("updateUserFamilyRole -> "+userId+ " "+isAdmin + " "+userIdToBeUpdated+ " "+familyId);
		if (null == userId)
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get("ERROR_USER_NULL")));
		if (null == familyId)
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get("ERROR_FAMILY_NULL")));
		if (null == userIdToBeUpdated)
			return ResponseEntity.badRequest()
					.body(new MessageResponse(messages.get("ERROR_USER_TO_UPDATE_NULL")));
		
		Family family = famService.getById(familyId);
		if (null != family) {
			User user  = userRepo.getById(userId);
			if(null != user){
				FamilyUserRoles adminUser = familyUsersRoleRepo.findRoleForUserInFamily(familyId,userId);
					if(null != adminUser && !adminUser.getRole().equalsIgnoreCase("ROLE_ADMIN")) {
						return ResponseEntity.badRequest()
								.body(new MessageResponse(messages.get("ERROR_USER_LACK_ADMIN_RIGHTS")));
					}
				
				Role userRole;
				if(isAdmin) {
					userRole = roleRepo.findByName(ERole.ROLE_ADMIN).get();
				}else {
					userRole = roleRepo.findByName(ERole.ROLE_USER).get();
				}
				
				FamilyUserRoles rolesOfUser = familyUsersRoleRepo.findRoleForUserInFamily(family.getFamilyId(), userIdToBeUpdated);
				rolesOfUser.setRole(userRole.getName().toString());
				familyUsersRoleRepo.save(rolesOfUser);
				
			}else {
				return ResponseEntity.badRequest()
						.body(new MessageResponse(messages.get("ERROR_USER_NOT_EXISTS")));
			}
		} else {
			return ResponseEntity.badRequest().body(
					new MessageResponse(messages.get("ERROR_FAMILY_NOTFOUND")));
		}

		LOGGER.info("updateUserFamilyRole(): " + family);
		LOGGER.traceExit();
		return ResponseEntity.ok(new MessageResponse(messages.get("UPDATE_USER_FAMILY_ROLE")));
	}
	
	/**
	 * @apiNote Used to get all roles for users in a family on basis of familyId
	 * @Param Integer familyId
	 * @return ResponseEntity (Family)
	 *
	 */
	@GetMapping("/allRolesInFamily/byFamilyId/{id}")
	public ResponseEntity getAllRolesOfUsersInFamilyById(@PathVariable(value = "id") Integer id) throws ResourceNotFoundException {
		LOGGER.traceEntry("getAllRolesOfUsersInFamilyById -> "+id);
		
		List<FamilyUserRoles> rolesOfUser = familyUsersRoleRepo.findAllUserRolesInFamily(id);
		if(null == rolesOfUser) {
			return ResponseEntity.badRequest().body(
					new MessageResponse(messages.get("ERROR_NO_RECORD_FOUND")));
		}
		LOGGER.info("getAllRolesOfUsersInFamilyById(): " + rolesOfUser);
		LOGGER.traceExit();
		return new ResponseEntity(rolesOfUser, HttpStatus.OK);
	}
	
	/**
	 * @apiNote Used to get role for users in a family on basis of familyId
	 * @Param Integer familyId
	 * @return ResponseEntity (Family)
	 *
	 */
	@GetMapping("/getUserRoleInFamily/byUserId/{id}/familyId/{familyId}")
	public ResponseEntity getRoleOfUsersInFamilyById(@PathVariable(value = "id") Integer userId,@PathVariable(value = "familyId") Integer familyId) throws ResourceNotFoundException {
		LOGGER.traceEntry("getRoleOfUsersInFamilyById -> "+userId + " "+familyId);
		
		FamilyUserRoles rolesOfUser = familyUsersRoleRepo.findRoleForUserInFamily(familyId, userId);
		if(null == rolesOfUser) {
			return ResponseEntity.badRequest().body(
					new MessageResponse(messages.get("ERROR_NO_RECORD_FOUND")));
		}
		LOGGER.info("getRoleOfUsersInFamilyById(): " + rolesOfUser);
		LOGGER.traceExit();
		return new ResponseEntity(rolesOfUser, HttpStatus.OK);
	}
}
