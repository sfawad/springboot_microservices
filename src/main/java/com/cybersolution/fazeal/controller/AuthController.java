package com.cybersolution.fazeal.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cybersolution.fazeal.exception.ResourceNotFoundException;
import com.cybersolution.fazeal.models.ERole;
import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.JwtResponse;
import com.cybersolution.fazeal.models.LoginRequest;
import com.cybersolution.fazeal.models.MessageResponse;
import com.cybersolution.fazeal.models.Role;
import com.cybersolution.fazeal.models.SignupRequest;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.repository.ImageRepository;
import com.cybersolution.fazeal.repository.RoleRepository;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.security.jwt.JwtUtils;
import com.cybersolution.fazeal.security.services.UserDetailsImpl;
import com.cybersolution.fazeal.service.EmailService;
import com.cybersolution.fazeal.service.impl.Messages;

@CrossOrigin(origins = "*", maxAge = 4300)
@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {
	private static final Logger logger = LogManager.getLogger(AuthController.class);

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	ImageRepository imageRepo;

	@Autowired
	Messages messages;
	
	@Autowired
	EmailService emailService;
	
	@Autowired
	private Environment environment;

	public AuthController(Environment environment) {
		this.environment = environment;
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
		
		logger.traceEntry("registerUser() --> " + signUpRequest);
		// @validation
		if (StringUtils.isEmpty(signUpRequest.getUsername())) {
			return ResponseEntity.badRequest().body(new MessageResponse(messages.get("ERROR_USER_NAME_NULL")));
		}
		if (StringUtils.isEmpty(signUpRequest.getEmail())) {
			return ResponseEntity.badRequest().body(new MessageResponse(messages.get("ERROR_EMAIL_NULL")));
		}
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return ResponseEntity.badRequest().body(new MessageResponse(messages.get("ERROR_USER_NAME")));
		}
		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity.badRequest().body(new MessageResponse(messages.get("ERROR_EMAIL_IN_USE")));
		}

		User user = new User(signUpRequest.getUsername(), signUpRequest.getEmail(),
		encoder.encode(signUpRequest.getPassword()), false);
		
		// user needs to create or join family to get enabled
		user.setEnabled(true);
		
		if(!StringUtils.isEmpty(signUpRequest.getDob())) {
			user.setDob(signUpRequest.getDob());
		}
		if(!StringUtils.isEmpty(signUpRequest.getFirstName())) {
			user.setFirstName(signUpRequest.getFirstName());
		}
		if(!StringUtils.isEmpty(signUpRequest.getLastName())) {
			user.setLastName(signUpRequest.getLastName());
		}

		String strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();

		if (strRoles == null) {
			Role userRole = roleRepository.findByName(ERole.ROLE_USER)
					.orElseThrow(() -> new RuntimeException(messages.get("ERROR_ROLE_NOT_FOUND")));
			roles.add(userRole);

		} else {
			switch (strRoles) {
			case "admin":
				Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
						.orElseThrow(() -> new RuntimeException(messages.get("ERROR_ROLE_NOT_FOUND")));
				roles.add(adminRole);

				break;
			default:
				Role userRole = roleRepository.findByName(ERole.ROLE_USER)
						.orElseThrow(() -> new RuntimeException(messages.get("ERROR_ROLE_NOT_FOUND")));
				roles.add(userRole);
			}
		}
		user.setCreated_at(getCurrentDateTime());
		user.setRoles(roles);
		userRepository.save(user);
		logger.info("registerUser(): " + user);
		logger.traceExit();
		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}

	@GetMapping("/users")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<List<User>> listAllUsers() {
		return ResponseEntity.ok(userRepository.findAll());
	}

	@GetMapping("/users/all/byUserId/{id}")
	public ResponseEntity<?> listAllUsersById(@PathVariable(value = "id") Integer userId) {
		logger.traceEntry("listAllUsers() --> " + userId);
		User user = userRepository.getById(userId);
		Set<Family> families = user.getFamilies();

		Map familyUserMap = new HashMap<>();
		for (Family family : families) {
			List<User> users = new ArrayList<User>();
			List<User> usersList = userRepository.getUsersListForAFamily(family.getFamilyId(), userId);
			for (User userc : usersList) {
				userc.setPassword("");
				users.add(userc);
			}

			if (null != users)
				familyUserMap.put(family.getFamilyName(), users);
		}
		logger.info("listAllUsersById(): " + familyUserMap);
		logger.traceExit();
		return ResponseEntity.ok(familyUserMap);
	}

	@GetMapping("/users/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<User> findUserById(@PathVariable(value = "id") Integer userId)
			throws ResourceNotFoundException {
		logger.traceEntry("findUserById() --> " + userId);
		User user = userRepository.findById(userId).orElseThrow(() -> {
			logger.info("findUserById() --> " + userId);
			return new ResourceNotFoundException(messages.get("ERROR_USER_NOT_FOUND") + userId);
		});
		logger.info("findUserById(): " + user);
		logger.traceExit();
		return ResponseEntity.ok().body(user);
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<User> updateUser(@PathVariable(value = "id") Integer userId,
			@RequestParam("username") String username, @RequestParam("email") String email,
			@RequestParam("password") String password) throws ResourceNotFoundException {
		logger.traceEntry("updateUser() --> " + userId + " " + username + " " + email + " " + password);
		User user = userRepository.findById(userId).orElseThrow(() -> {
			logger.info("updateUser: failed with userId: " + userId);
			return new ResourceNotFoundException(messages.get("ERROR_USER_NOT_FOUND") + userId);
		});

		if (username != null) {
			user.setUsername(username);
		}

		if (email != null) {
			user.setEmail(email);
		}

		if (!password.equals("")) {
			user.setPassword(encoder.encode(password));
		}

		final User updatedUser = userRepository.save(user);
		logger.info("updateUser(): " + updatedUser);
		logger.traceExit();
		return ResponseEntity.ok(updatedUser);
	}

	@PutMapping("/updateEnbaled/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<User> updateUserEnabled(@PathVariable(value = "id") Integer userId,
			@RequestParam("enabled") boolean enabled, @RequestParam("userIdToBeUpdated") Integer userIdToBeUpdated)
			throws ResourceNotFoundException {
		logger.traceEntry("updateUserEnabled() --> " + userId + " " + enabled + " " + userIdToBeUpdated);
		User user = userRepository.findById(userIdToBeUpdated).orElseThrow(
				() -> new ResourceNotFoundException(messages.get("ERROR_USER_NOT_FOUND") + userIdToBeUpdated));

		if (enabled) {
			user.setEnabled(enabled);
		}
		final User updatedUser = userRepository.save(user);
		logger.info("updateUserEnabled(): " + updatedUser);
		logger.traceExit();
		return ResponseEntity.ok(updatedUser);
	}

	@Transactional
	@DeleteMapping("/delete/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public Map<String, Boolean> deleteUser(@PathVariable(value = "id") Integer userId) {
		logger.traceEntry("deleteUser() --> " + userId);
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new RuntimeException(messages.get("ERROR_USER_NOT_FOUND") + userId));

		userRepository.delete(user);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		logger.info("user deleted for userID : " + userId);
		logger.traceExit();
		return response;
	}

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
		logger.traceEntry("authenticateUser() --> " + loginRequest);
		Optional<User> opUser = userRepository.findByUsername(loginRequest.getUsername());
		
		if(null != opUser && !opUser.isEmpty()) {
			User user = opUser.get();
			if (user != null && user.isEnabled()) {
				Authentication authentication;
				try {
				authentication = authenticationManager.authenticate(
						new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
				}catch(Exception ec) {
					return ResponseEntity.status(401).body(new MessageResponse(messages.get("ERROR_INVALID_USER_PASSWORD")));
				}
				SecurityContextHolder.getContext().setAuthentication(authentication);
				String jwt = jwtUtils.generateJwtToken(authentication);
				UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
				List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
						.collect(Collectors.toList());
	
				logger.traceExit();
				return ResponseEntity.ok(new JwtResponse(jwt, null, userDetails.getId(), userDetails.getUsername(),
						userDetails.getEmail(), roles));
				
			} else {
				return ResponseEntity.status(401).body(new MessageResponse(messages.get("ERROR_USER_UNAPPROVED")));
			}
		}else {
			return ResponseEntity.status(401).body(new MessageResponse(messages.get("ERROR_USER_NOT_EXISTS")));
		}
	}
	
	@PutMapping("/forgot/byEmail/{email}")
	public ResponseEntity<?> forgotPassword(@PathVariable("email") String email) {
		logger.traceEntry("forgotPassword() --> " + email);
		if(StringUtils.isEmpty(email)) {
			return ResponseEntity.badRequest().body(new MessageResponse(messages.get("ERROR_EMAIL_NULL")));
		}
		User user = userRepository.getUserFromEmail(email);
		if(null == user) {
			return ResponseEntity.badRequest().body(new MessageResponse(messages.get("ERROR_USER_NOT_EXISTS")));
		}
		String token = UUID.randomUUID().toString();
		user.setReset_token(token);
		user.setUpdated_at(getCurrentDateTime());
		userRepository.save(user);

		// Email message
		SimpleMailMessage passwordResetEmail = new SimpleMailMessage();
		passwordResetEmail.setFrom(messages.get("PASSWORD_EMAIL_FROM"));
		passwordResetEmail.setTo(user.getEmail());
		passwordResetEmail.setSubject(messages.get("PASSWORD_EMAIL_SUBJECT_RESET"));
		passwordResetEmail.setText(messages.get("PASSWORD_EMAIL_RESET_TEXT") + token);

		emailService.sendEmail(passwordResetEmail);

		logger.traceExit();
		return ResponseEntity.ok(new MessageResponse(messages.get("PASSWORD_EMAIL_SENT_SUCCESS")));

	}
	
	@GetMapping("/verifyToken/{token}")
	public Map verifyResetToken(@PathVariable(value = "token") String token) {
		logger.traceEntry("verifyRestToken() --> " + token);
		Map<String,Boolean> verifyMap = new HashMap<>();
		if(StringUtils.isEmpty(token)) {
			logger.info(messages.get("PASSWORD_ERROR_USER_TOKEN_EMPTY"));
			logger.traceExit();
			verifyMap.put("isValid", false);
			return verifyMap;
		}
		
		User user = userRepository.getUserFromToken(token);
		if(null == user) {
			logger.info(messages.get("PASSWORD_ERROR_USER_TOKEN_INVALID"));
			logger.traceExit();
			verifyMap.put("isValid", false);
			return verifyMap;
		}else {
			verifyMap.put("isValid", true);
			logger.traceExit();
			return verifyMap;
		}
		
	}
	
	@PutMapping("/reset")
	public ResponseEntity<?> resetPassword(@RequestParam("token") String token,
			@RequestParam("password") String password) {
		logger.traceEntry("resetPassword() --> " + token);
		Long reset_token_time = Long.valueOf(environment.getProperty("token-expiry"));
		if(StringUtils.isEmpty(token)) {
			return ResponseEntity.badRequest().body(new MessageResponse(messages.get("PASSWORD_ERROR_USER_TOKEN_EMPTY")));
		}
		
		User user = userRepository.getUserFromToken(token);
		if(null == user) {
			return ResponseEntity.badRequest().body(new MessageResponse(messages.get("PASSWORD_ERROR_USER_TOKEN_INVALID")));
		}
		
		SimpleDateFormat formatter  = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		String dateInString = user.getUpdated_at();
		Long updated_time = null;
		try {
			Date date = formatter.parse(dateInString);
			updated_time = date.getTime();
			updated_time = Long.sum(updated_time, reset_token_time);
		}catch(Exception ec) {
			logger.error(ec.getMessage());
		}
		Long current_time = System.currentTimeMillis();
		if(null != updated_time  && updated_time < current_time) {
			return ResponseEntity.status(401).body(new MessageResponse(messages.get("PASSWORD_TOKEN_EXPIRED")));
		}
		user.setReset_token(null);
		user.setEnabled(true);
		user.setUpdated_at(getCurrentDateTime());
		user.setPassword(encoder.encode(password));

		// Email message
		SimpleMailMessage passwordResetEmail = new SimpleMailMessage();
		passwordResetEmail.setFrom(messages.get("PASSWORD_EMAIL_FROM"));
		passwordResetEmail.setTo(user.getEmail());
		passwordResetEmail.setSubject(messages.get("PASSWORD_EMAIL_SUBJECT_RESET_DONE"));
		passwordResetEmail.setText(messages.get("PASSWORD_EMAIL_RESET_DONE_TEXT"));

		emailService.sendEmail(passwordResetEmail);

		logger.traceExit();
		return ResponseEntity.ok(new MessageResponse(messages.get("PASSWORD_UPDATED_SUCCESS")));

	}
	
	public String getCurrentDateTime() {
		SimpleDateFormat formatter  = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		Date date=  new Date(System.currentTimeMillis());
		return formatter.format(date);
	}
}