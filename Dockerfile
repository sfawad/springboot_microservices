#FROM adoptopenjdk/openjdk11:alpine-jre
#VOLUME /main-app
#ADD target/fazeal-springboot-microservice-0.0.1-SNAPSHOT.jar app.jar
#EXPOSE 8080
#ENTRYPOINT ["java", "-jar","/app.jar"]

####  Done, package it with Maven.
####mvn clean package

#### Run the project
####java -jar target/spring-boot-web.jar

#### For Java 8, try this
### FROM openjdk:8-jdk-alpine

#### For Java 11, try this
FROM adoptopenjdk/openjdk11:alpine-jre

## Refer to Maven build -> finalName
ARG JAR_FILE=target/fazeal-springboot-microservice-0.0.1-SNAPSHOT.jar

## cd /opt/app
WORKDIR /opt/app

## cp target/spring-boot-web.jar /opt/app/app.jar
COPY ${JAR_FILE} app.jar

## java -jar /opt/app/app.jar
ENTRYPOINT ["java","-jar","app.jar"]